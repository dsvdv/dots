vim.cmd [[
	colorscheme nord
	set nocompatible
	filetype plugin on
	syntax on
	]]
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.opt.incsearch = true
vim.opt.hlsearch = true

vim.opt.showmode = true
vim.opt.ttyfast = true
vim.opt.scrolloff = 999
vim.opt.clipboard = unnamed
vim.opt.cursorline = true
vim.opt.list = true
vim.opt.linebreak = true

vim.opt.number = true
vim.opt.relativenumber = true

vim.opt.tabstop = 2
vim.opt.shiftwidth = 2
vim.opt.softtabstop = 2
vim.opt.expandtab = true
vim.opt.autoindent = true

vim.opt.wrap = true

-- nvim-tree
require("nvim-tree").setup({
  sort = {
    sorter = "case_sensitive",
  },
  view = {
    width = 30,
  },
  renderer = {
    group_empty = true,
  },
  filters = {
    dotfiles = true,
  },
})

-- lualine
require("lualine").setup {
  options = {
	  theme  = "nord",
	  icons_enabled = true,
	}
}

-- vim.g.vimwiki_list = {{path = '~/.vimwiki', syntax = 'markdown', ext = '.md'}}



vim.cmd [[ function! s:goyo_enter()
  lua require('lualine').hide()
  Limelight!!
endfunction

function! s:goyo_leave()
  lua require('lualine').hide({unhide=true})
  Limelight!!
endfunction

autocmd! User GoyoEnter call <SID>goyo_enter()
autocmd! User GoyoLeave call <SID>goyo_leave() ]]

