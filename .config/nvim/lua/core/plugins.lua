local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
  'shaunsingh/nord.nvim',
  'nvim-tree/nvim-tree.lua',
  'junegunn/goyo.vim',
  'junegunn/limelight.vim',
  'nvim-treesitter/nvim-treesitter',
  'brenoprata10/nvim-highlight-colors',
  {
    'nvim-lualine/lualine.nvim',
    dependencies = 
		  {
				'nvim-tree/nvim-web-devicons'
			},
  },
	{
    'windwp/nvim-autopairs',
    event = "InsertEnter",
    config = true
    -- use opts = {} for passing setup options
    -- this is equalent to setup({}) function
},
   {
    'nvim-telescope/telescope.nvim', tag = '0.1.8',
-- or                              , branch = '0.1.x',
      dependencies = { 'nvim-lua/plenary.nvim' }
    }

})

