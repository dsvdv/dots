vim.g.mapleader = ' '

-- moving around
vim.keymap.set('n', 'K', '10k')
vim.keymap.set('n', 'J', '10j')
vim.keymap.set('n', '<c-k>', ':move -2<cr>')
vim.keymap.set('n', '<c-j>', ':move +1<cr>')
vim.keymap.set('i', 'jk', "<esc>")
vim.keymap.set('i', 'ii', "<esc>")
vim.keymap.set('n', '<leader>S', ':so %<cr>')
vim.keymap.set('n', '<leader>n', ':NvimTreeFindFileToggle<cr>')
vim.keymap.set('n', '<leader>hl', ':HighlightColorsToggle<cr>')
vim.keymap.set('n', '<leader>sg', ':Goyo<cr>')
vim.keymap.set('n', '<leader>sl', ':LimeLight!!<cr>')

vim.keymap.set('n', '<leader>ff', require('telescope.builtin').find_files, {})
vim.keymap.set('n', '<leader>fg', require('telescope.builtin').live_grep, {})

vim.keymap.set('n', '<leader><F1>', ':setlocal spell! spelllang=en_us<cr>')
vim.keymap.set('n', '<leader><F2>', ':setlocal spell! spelllang=sv<cr>')

vim.keymap.set('n', '<leader>fn', ']s')
vim.keymap.set('n', '<leader>fp', '[s')
vim.keymap.set('n', '<leader>fc', 'z=')
vim.keymap.set('n', '<leader>fa', 'zg')
