export PATH=$PATH:$HOME/.config/scripts								# where my scripts live
export PATH=$PATH:$HOME/.local/bin									# where my binaries live
export PATH=$PATH:$HOME/.local/fonts								# where my fonts live
export PATH=$PATH:$HOME/.cargo/bin									# cargo dir

# ----------------
# export
export ZDOTDIR="$HOME/.config/zsh"									# zsh dir
export MAILCAPS=$HOME/.config/.mailcap								# mailcap dir
export LESSHISTFILE='$HOME/.local/share/lesshst'					# move less history
export TERMINAL='alacritty'											# set default terminal
export BROWSER='qutebrowser'										# set default browser
export EDITOR='nvim'												# set default text editor

export PASSWORD_STORE_DIR=$HOME/.local/share/.password-store		# .password-store dir

# ----------------
# fix for keepassxc
export QT_SCALE_FACTOR=1.2
export QT_QPA_PLATFORMTHEME= keepassxc

# ----------------
# pfetch
export PF_INFO='title os kernel wm pkgs shell editor memory palette'
export PF_COLOR='0'

#export ROFI_PASS_CONFIG="$HOME/.config/rofi-pass/config" rofi-pass
